package org.nico.noson.scanner.plant;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/** 
 * 
 * @author nico
 * @version createTime：2018年4月9日 下午9:07:08
 */

public class ByteArrayPlant extends AbstractPlant{

	@Override
	public List<Byte> get() {
		return new ArrayList<Byte>();
	}

}
