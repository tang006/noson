package org.nico.noson.scanner.plant;

import java.util.LinkedHashMap;
import java.util.Map;

/** 
 * 
 * @author nico
 * @version createTime：2018年4月9日 下午9:07:08
 */

public class MapPlant extends AbstractPlant{

	@Override
	public Map get() {
		return new LinkedHashMap<String, Object>();
	}

}
