package org.nico.noson.scanner.plant;

import java.util.ArrayList;
import java.util.List;

/** 
 * 
 * @author nico
 * @version createTime：2018年4月9日 下午9:07:08
 */

public class ShortObjArrayPlant extends AbstractPlant{

	@Override
	public List<Short> get() {
		return new ArrayList<Short>();
	}

}
