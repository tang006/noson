package org.nico.noson.test.entity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nico.noson.test.entity.ComplexEntity.Type;

/** 
 * 复杂类型，用于性能测试调优使用
 * 
 * @author nico
 * @version createTime：2018年4月7日 下午4:38:31
 */
public class ASMTestEntity {

	private int id;
	
	private String str;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	
}
