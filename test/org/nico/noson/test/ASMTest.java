package org.nico.noson.test;

import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import org.nico.noson.exception.NosonException;
import org.nico.noson.test.entity.ASMTestEntity;
import org.nico.noson.test.entity.ComplexEntity;
import org.nico.noson.test.entity.ComplexEntity.Type;
import org.nico.noson.util.asm.ASMClassProxyBuilder;
import org.nico.noson.util.asm.AbstractASMClassProxy;

/** 
 * 
 * @author nico
 * @version createTime：2018年4月22日 上午1:06:23
 */

public class ASMTest {

	public static void main(String[] args) throws IOException, InstantiationException, IllegalAccessException, NoSuchFieldException, SecurityException, NosonException {

		ASMClassProxyBuilder asmProxyBuilder = new ASMClassProxyBuilder();

		AbstractASMClassProxy proxy = asmProxyBuilder.getASMClassProxy(ComplexEntity.class);
		int count = 1000 * 10000;
		long start = System.currentTimeMillis();
		Object entity = null;
		for(int index = 0; index < count; index ++){
			entity = new ComplexEntity();
			proxy.set(entity, "id", 1);
			proxy.set(entity, "c", 'o');
			proxy.set(entity, "inte", 5);
			proxy.set(entity, "str", "321");
			proxy.set(entity, "num", 12.5);
			proxy.set(entity, "date", new Date());
			proxy.set(entity, "type", Type.Type1);
			proxy.set(entity, "list", Arrays.asList("a","b","c"));
			proxy.set(entity, "map", new HashMap<String, Object>());
			proxy.set(entity, "set", new HashSet<Object>());
			proxy.set(entity, "type", Type.Type1);
			proxy.set(entity, "bigDecimal", new BigDecimal(1));
			proxy.set(entity, "strs", new String[]{"a","b","c"});
			proxy.set(entity, "floats", new float[]{1,2,3,4});
		}
		System.out.println(count + "次对象赋值用时：" + (System.currentTimeMillis() - start) + "(ms)");
		System.out.println(entity);
		
		
		
//		System.out.println(proxy.get(entity, "id"));
//		System.out.println(proxy.get(entity, "c"));
//		System.out.println(proxy.get(entity, "inte"));
//		System.out.println(proxy.get(entity, "str"));
//		System.out.println(proxy.get(entity, "num"));
//		System.out.println(proxy.get(entity, "date"));
//		System.out.println(proxy.get(entity, "type"));
//		System.out.println(proxy.get(entity, "list"));
//		System.out.println(proxy.get(entity, "map"));
//		System.out.println(proxy.get(entity, "set"));
//		System.out.println(proxy.get(entity, "type"));
//		System.out.println(proxy.get(entity, "bigDecimal"));
//		System.out.println(proxy.get(entity, "strs"));
//		System.out.println(proxy.get(entity, "floats"));
		
//		Field field = ComplexEntity.class.getDeclaredField("id");
//		Field field1 = ComplexEntity.class.getDeclaredField("str");
//		for(int index = 0; index < count; index ++){
//			ComplexEntity entity = new ComplexEntity();
//
//			field.setAccessible(true);
//			field.set(entity, 1);
//			field1.setAccessible(true);
//			field1.set(entity, "str");
//		}
	}
}
